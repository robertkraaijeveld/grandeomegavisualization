Rails.application.routes.draw do
  get 'home/index'
  root 'home#index'

  get 'home/kmeans/:percentage', to: 'home#kmeans'
  get 'home/successrate/:percentage', to: 'home#successrate'  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
 